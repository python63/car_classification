import cv2
from config import car_config as config
from MyModules import ImageToArrayPreprocessor
from MyModules import AspectAwarePreprocessor
from MyModules import MeanPreprocessor
from car_mapping import Car_Mapping
import numpy as np
import mxnet as mx
import argparse
import pickle
import imutils
import os

vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-i", "--image",required=True, help="image to classify")
vArgPar.add_argument("-c", "--checkpoints", required=True)
vArgPar.add_argument("-p", "--prefix", required=True)
vArgPar.add_argument("-e", "--epoch", type=int, required=True)
vArgPar.add_argument("-s", "--sample-size", type=int, default=10)

args = vars(vArgPar.parse_args())

def car_classification(Image,model,ImgPreprop,MeanPrepro,AspPrepro,car_name_dataset):
	vImage_Copy = Image.copy()
	vImage_Copy = imutils.resize(vImage_Copy, width=min(500, vImage_Copy.shape[1]))
	
	vImage_process = ImgPreprop.preprocess(MeanPrepro.preprocess(AspPrepro.preprocess(Image)))
	vImage_process = np.expand_dims(vImage_process, axis=0)
	#Klasyfikacja
	vCar_Prediction = model.predict(vImage_process)[0]
	#Predykcja
	vCar_Index = np.argsort(vCar_Prediction)[::-1][:1]

	vPredicted_Car = car_name_dataset.predicted_label(vCar_Index[0])
	#Stworzenie stringa do wyswietlenia na obrazie
	vPredicted_Percentage = round(vCar_Prediction[vCar_Index[0]] * 100,2)
	vPredicted_Percentage = str(vPredicted_Percentage) + str("%")

	return (vPredicted_Car,vPredicted_Percentage)


def main():
	checkpointsPath = os.path.sep.join([args["checkpoints"],args["prefix"]])

	#Tworzenie modelu
	vModel = mx.model.FeedForward.load(checkpointsPath,args["epoch"])
	vModel = mx.model.FeedForward(ctx=[mx.gpu(0)],symbol=vModel.symbol,arg_params=vModel.arg_params,aux_params=vModel.aux_params)

	# Preprocessory
	vAspAwPrep = AspectAwarePreprocessor(width=224, height=224)
	vMeanPrep = MeanPreprocessor(config.R_MEAN, config.G_MEAN, config.B_MEAN)
	vImgtArrPrep = ImageToArrayPreprocessor(dataFormat="channels_first")
	vCar_Id = Car_Mapping()

	# Wczytanie obrazu
	vImage = cv2.imread(args["image"])

	(vPredCar, vPredPerc) = car_classification(vImage,vModel,vImgtArrPrep,vMeanPrep,vAspAwPrep,vCar_Id)
	#Putting string on the image
	cv2.putText(vImage, str(vPredCar), (10, 30), cv2.FONT_HERSHEY_SIMPLEX,0.6, (0, 255, 0), 2)
	cv2.putText(vImage, str(vPredPerc), (10, 60), cv2.FONT_HERSHEY_SIMPLEX,0.6, (0, 255, 0), 2)
	# show the image
	cv2.imshow("Image", vImage)
	cv2.waitKey(0)

if __name__ == "__main__":
	main()



    	
