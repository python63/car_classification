from config import car_config as config
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import progressbar
import pickle
import os


vRows = open(config.LABELS_PATH).read()
vRows = vRows.strip().split("\n")[1:]
trainPaths = []
trainLabels = []

#Baza danych marek i modeli + sciezki do nich
for row in vRows:
	(tFilename, tMake, tModel) = row.split(",")[:3]
	tFilename = tFilename[tFilename.rfind("/") + 1:]
	trainPaths.append(os.sep.join([config.IMAGES_PATH, filename]))
	trainLabels.append("{}:{}".format(tMake, tModel))

numVal = int(len(trainPaths) * config.NUM_VAL_IMAGES)
numTest = int(len(trainPaths) * config.NUM_TEST_IMAGES)

#Encodowanie wartosci
vLabEnc = LabelEncoder().fit(trainLabels)
trainLabels = vLabEnc.transform(trainLabels)

#Podzial na zbiory
(trainPaths, valPaths, trainLabels, valLabels) = train_test_split(trainPaths, trainLabels, test_size=numVal,stratify=trainLabels)

(trainPaths, testPaths, trainLabels, testLabels) = train_test_split(trainPaths, trainLabels, test_size=numTest,stratify=trainLabels)

datasets = [("train", trainPaths, trainLabels, config.TRAIN_MX_LIST),("val", valPaths, valLabels, config.VAL_MX_LIST),("test", testPaths, testLabels, config.TEST_MX_LIST)]

#Zapis sciezek i labeli
for (dType, paths, labels, outputPath) in datasets:

	tFile = open(outputPath, "w")

	for (i, (path, label)) in enumerate(zip(paths, labels)):
		row = "\t".join([str(i), str(label), path])
		tFile.write("{}\n".format(row))

	tFile.close()

vFile = open(config.LABEL_ENCODER_PATH, "wb")
vFile.write(pickle.dumps(vLabEnc))
vFile.close()
