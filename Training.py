from config import car_config as config
import mxnet as mx
import argparse
import logging
import os

vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-v", "--vgg")
vArgPar.add_argument("-c", "--checkpoints")
vArgPar.add_argument("-p", "--prefix")

args = vars(vArgPar.parse_args())

logging.basicConfig(level=logging.DEBUG,filename="training_{}.log".format(int(0)),filemode="w")

batchSize = config.BATCH_SIZE * config.NUM_DEVICES

vIterTrain = mx.io.ImageRecordIter(
	path_imgrec=config.TRAIN_MX_REC,
	data_shape=(3, 224, 224),
	batch_size=batchSize,
	rand_crop=True,
	rand_mirror=True,
	rotate=15,
	max_shear_ratio=0.1,
	mean_r=config.R_MEAN,
	mean_g=config.G_MEAN,
	mean_b=config.B_MEAN,
	preprocess_threads=config.NUM_DEVICES * 2)

vIterVal = mx.io.ImageRecordIter(
	path_imgrec=config.VAL_MX_REC,
	data_shape=(3, 224, 224),
	batch_size=batchSize,
	mean_r=config.R_MEAN,
	mean_g=config.G_MEAN,
	mean_b=config.B_MEAN)

opt = mx.optimizer.SGD(learning_rate=5e-5, momentum=0.9, wd=0.0005,
	rescale_grad=1.0 / batchSize)
ctx = [mx.gpu(3)]

checkpointsPath = os.path.sep.join([args["checkpoints"],
	args["prefix"]])
argParams = None
auxParams = None
allowMissing = False

(symbol, argParams, auxParams) = mx.model.load_checkpoint(args["vgg"], 0)
allowMissing = True

layers = symbol.get_internals()
net = layers["drop7_output"]

net = mx.sym.FullyConnected(data=net,num_hidden=config.NUM_CLASSES, name="fc8")
net = mx.sym.SoftmaxOutput(data=net, name="softmax")

argParams = dict({k:argParams[k] for k in argParams if "fc8" not in k})

batchEndCBs = [mx.callback.Speedometer(batchSize, 50)]
epochEndCBs = [mx.callback.do_checkpoint(checkpointsPath)]
metrics = [mx.metric.Accuracy(), mx.metric.TopKAccuracy(top_k=5),
	mx.metric.CrossEntropy()]

#Trenowanie sieci
vModel = mx.mod.Module(symbol=net, context=ctx)
vModel.fit(
	vIterTrain,
	eval_data=vIterVal,
	num_epoch=65,
	begin_epoch=0,
	initializer=mx.initializer.Xavier(),
	arg_params=argParams,
	aux_params=auxParams,
	optimizer=opt,
	allow_missing=allowMissing,
	eval_metric=metrics,
	batch_end_callback=batchEndCBs,
	epoch_end_callback=epochEndCBs)
